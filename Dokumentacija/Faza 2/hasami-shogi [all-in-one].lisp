#| =========================================================================|#
#| ======================== game-display.lisp :begin =======================|#

(defun display-game (game)
	(progn 
		(format t "~%")
		(display-table (car game))
		(display-player-status (caddr game))))

(defun display-gameover (table winner)
	(progn
		(display-table table)
		(format t "~%~%Kraj igre => Pobednik je: ~a!" winner)))

(defun display-table (table)
	(mapcar #'display-row (append 
		(list (create-columns-indicator-row (length table)))
		(insert-rows-indicator table (length table)))))

(defun display-player-status (player)
	(format t "~%~%Igrac ~a je na potezu: " player))

(defun display-invalid-move-msg ()
	(format t "~%~% ==== Los potez! ==== ~%~%")
)

(defun create-columns-indicator-row (dimension)
	(cond 
		((zerop dimension) (list #\#)) 
		(t (append (create-columns-indicator-row (- dimension 1)) (list dimension)))))

(defun insert-rows-indicator (table dimension)
	(cond
		((null table) nil)
		(t (append (list (insert-row-indicator (car table) (- dimension (length table)))) (insert-rows-indicator (cdr table) dimension)))))

(defun insert-row-indicator (row offset)
	(cons (code-char (+ (char-code #\A) offset)) row))

(defun display-row (row)
	(progn 
		(mapcar (lambda (element) (format t "~a " element)) row))
	    (format t "~%"))

#| ======================== game-display.lisp :end =========================|#
#| =========================================================================|#
#| =========================================================================|#
#| ======================== game-move-sandwich.lisp :begin =================|#

(defun make-sandwich (table row-end column-end figure)
	(let* (
		(cleared-right (make-sandwich-right table row-end column-end figure)) 
		(cleared-left (make-sandwich-left cleared-right row-end column-end figure)) 
		(cleared-top (make-sandwich-top cleared-left row-end column-end figure))
		(cleared-bottom (make-sandwich-bottom cleared-top row-end column-end figure)))
		cleared-bottom))

;;; Primenjuje sendvic obradu na elementima udesno od odigrane figure
(defun make-sandwich-right (table row-end column-end figure)
	(let ((row (make-sandwich-in-list (get-table-row table row-end) column-end figure)))
		(replace-element row row-end table)))

;;; Primenjuje sendvic obradu nad elementima ulevo od odigrane figure
(defun make-sandwich-left (table row-end column-end figure)
	(let ((row (reverse (make-sandwich-in-list (reverse (get-table-row table row-end)) (- (length table) column-end 1) figure))))
		(replace-element row row-end table)))

;;; Primenjuje sendvic obradu nad elementima iznad od odigrane figure
(defun make-sandwich-top (table row-end column-end figure)
	(let ((column (reverse (make-sandwich-in-list (reverse (get-table-column table column-end)) (- (length table) row-end 1) figure))))
		(matrix-transpose (replace-element column column-end (matrix-transpose table)))))

;;; Primenjuje sendvic obradu nad elementima ispod od odigrane figure
(defun make-sandwich-bottom (table row-end column-end figure)
	(let ((column (make-sandwich-in-list (get-table-column table column-end) row-end figure)))
		(matrix-transpose (replace-element column column-end (matrix-transpose table)))))

;;; Sklanja protivnicke figure u sendvicu ako je sendvic ostvaren. Uopsteni algoritam, radi nad listom.
(defun make-sandwich-in-list (row column-index figure)
	(cond
		((not (equalp (nth column-index row) figure)) row)
		(t (let 
			((sandwich-count (check-sandwich (subseq row column-index (length row)) figure)))
			(if (> sandwich-count 0) (clear-fields row (1+ column-index) (+ 1 column-index sandwich-count)) row)))))

;;; Prebrojava koliko protivnickih figura ima u potencialnom sendvicu
;	Ako je broj negativan => sendvic nije formiran.
(defun check-sandwich (row figure)
  (cond
    ((null (cadr row)) -10000)
    ((equalp (cadr row) figure) 0)
    ((equalp (cadr row) '-) -10000)
    ((not (equalp (cadr row) figure)) (1+ (check-sandwich (cdr row) figure)))))

(defun clear-fields (row start end)
	(cond
		((null row) nil)
		((zerop end) row)
		((zerop start) (cons '- (clear-fields (cdr row) start (1- end))))
		(t (cons (car row) (clear-fields (cdr row) (1- start) (1- end))))))

#| ======================== game-move-sandwich.lisp :end ===================|#
#| =========================================================================|#
#| =========================================================================|#
#| ======================== game-move.lisp :begin ==========================|#


;#| Validacije #|row-start column-start row-end column-end|#|#
(defun check-valid-move (game move)
	(and 
		(check-start-position game (nth 0 move) (nth 1 move)) 
		(or (check-move-position game (nth 0 move) (nth 1 move) (nth 2 move) (nth 3 move))
			(check-jump-move (get-table game) (nth 0 move) (nth 1 move) (nth 2 move) (nth 3 move)))))

(defun check-start-position (game row column)
	(eq (get-current-player game) (get-field (get-table game) row column)))

(defun check-move-position (game row-start column-start row-end column-end)
	(and 
		(check-empty-position (get-table game) row-end column-end)
		(check-no-figure-between (get-table game) row-start column-start row-end column-end)))

(defun check-empty-position (table row column)
	(eq (get-field table row column) '-))

;#| Needs refactoring |#
(defun check-no-figure-between (table row-start column-start row-end column-end)
	(cond
		((eq row-start row-end)
			(cond ((eq (abs (- column-end column-start)) 1) t)
				(t (check-same-elements-in-list (subseq (get-table-row table row-start) (1+ (min column-start column-end)) (max column-end column-start)) '-))))
		((eq column-start column-end)
			(cond ((eq (abs (- row-start row-end)) 1) t)
				(t (check-same-elements-in-list (subseq (get-table-column table column-start) (1+ (min row-start row-end)) (max row-start row-end)) '-))))))
;#| Validacije kraj |#

;#| Move: row-start column-start row-end column-end |#
(defun make-a-move (game move)
	(if (check-valid-move game move)
		(let* (
				(move-played (set-field (clear-field (get-table game) (nth 0 move) (nth 1 move)) (nth 2 move) (nth 3 move) (get-current-player game)))
				(sandwich-move-played (make-sandwich move-played (nth 2 move) (nth 3 move) (get-current-player game)))
			  )			
				(create-game-state sandwich-move-played (get-player game) (next-player game)))))

;#| Input je oblika: ((row-start column-start) (row-end column-end)) 
;   Dodatna validacija|#
(defun check-input (input)
	(and 
		(listp input)
		(eq (length input) 2)
		(listp (car input))
		(listp (cadr input))
		(eq (length (car input)) 2)
		(eq (length (cadr input)) 2)))

;#| Konvetuje input oblika A 1 C 1 => 0 0 2 0|#
(defun parse-move (move)
	(cond
		((null move) nil)
		(t (list (index-of-char (nth 0 move)) (1- (nth 1 move)) (index-of-char (nth 2 move)) (1- (nth 3 move))))))

(defun index-of-char (char-input)
	(- (char-code char-input) (char-code #\A)))

(defun check-jump-move (table row-start column-start row-end column-end) 
  (cond
  	((null table) nil)
    (t (or 
        (check-jump-move-vertical table row-start column-start row-end column-end)
        (check-jump-move-horizontal table row-start column-start row-end column-end)))))

(defun check-jump-move-vertical (table row-start column-start row-end column-end)
  (and 
    (eq column-start column-end)
    (eq (abs (- row-start row-end)) 2) 
    (eq (get-field table row-end column-start) '-)))

(defun check-jump-move-horizontal (table row-start column-start row-end column-end)
    (and 
    (eq row-start row-end)
    (eq (abs (- column-start column-end)) 2) 
    (eq (get-field table row-start column-end) '-)))

#| ======================== game-move.lisp :end ============================|#
#| =========================================================================|#
#| =========================================================================|#
#| ======================== game-over.lisp :begin ==========================|#

;;; Testira da li je igrac pobedio
(defun gameover-check-if-won (table figure)
    (or
        (--gameover-check-nofigures table (get-oponent-figure figure))   
        (--gameover-check-win-vert table figure)
        (--gameover-check-win-diag table figure)
    )
)

;;; Testira da li je neko pobedio. Vraca pobednika ili nil
(defun gameover-check-if-any-won (table)
    (cond ((gameover-check-if-won table 'X) 'X)
          ((gameover-check-if-won table 'O) 'O)
    )
)

;;; Uslov: Spojio 5 figura u vertikali
(defun --gameover-check-win-vert (table figure)
    (cond ((equal figure 'X) (matrix-check-has-n-successive (matrix-transpose (cddr table)) figure 5))
          ((equal figure 'O) (matrix-check-has-n-successive (matrix-transpose (cddr (reverse table))) figure 5))
    )
)

; ;;; Uslov: Spojio 5 figura u horizontali
; (defun --gameover-check-win-horiz (table figure)
;     (cond ((equal figure 'X) (matrix-check-has-n-successive (cddr table) figure 5))
;           ((equal figure 'O) (matrix-check-has-n-successive (cddr (reverse table)) figure 5))
;     )
; )

;;; Uslov: Spojio 5 figura u nekoj dijagonali
(defun --gameover-check-win-diag (table figure)
    (or (--gameover-check-win-fdiag table figure) (--gameover-check-win-bdiag table figure))
)

;;; PodUslov: Spojio 5 figura u forward-diagonali /
; TODO: optimise
(defun --gameover-check-win-fdiag (table figure)
    (cond ((equal figure 'X) (matrix-check-has-n-successive (matrix-get-fdiag-view (cddr table)) figure 5))
          ((equal figure 'O) (matrix-check-has-n-successive (matrix-get-fdiag-view (reverse (cddr (reverse table)))) figure 5))
    )
)

;;; PodUslov: Spojio 5 figura u backward-dijagonali \
; TODO: optimise
(defun --gameover-check-win-bdiag (table figure)
    (cond ((equal figure 'X) (matrix-check-has-n-successive (matrix-get-bdiag-view (cddr table)) figure 5))
          ((equal figure 'O) (matrix-check-has-n-successive (matrix-get-bdiag-view (reverse (cddr (reverse table)))) figure 5))
    )
)

;;; Uslov: Ima manje od 5 figura
; TODO: optimise
(defun --gameover-check-nofigures (table figure)
    (> 5 (count-player-figures table figure))
)

(defun count-player-figures (table figure)
    (reduce #'+ (mapcar (lambda (table-row) (count figure table-row)) table)))

#| ======================== game-over.lisp :end ============================|#
#| =========================================================================|#
#| =========================================================================|#
#| ======================== game-states.lisp :begin ========================|#

;;Vraca: ( (game move) (game move) )
(defun generate-next-possible-states (game)
    (reduce #'append 
        (mapcar (lambda (current-position) (get-posible-states-from-position game current-position)) 
                        (game-get-player-figures-positions (get-player game) (get-table game))
        )
    )
)

;; === Formiranje liste svih mogucih poteza za jednu figuru ===
(defun get-posible-states-from-position (game start-position)
    (--get-posible-states-from-pos-Table game start-position (get-table-dimension game) (get-table-dimension game)) ;;2lazy4let
)

(defun --get-posible-states-from-pos-Table (game start-position row-counter table-size)
    (cond ((= row-counter -1) nil)
          (t (append    (--get-posible-states-from-pos-Table game start-position (1- row-counter) table-size)
                        (--get-posible-states-from-pos-Row game start-position row-counter table-size)
          ))
    )
)

(defun --get-posible-states-from-pos-Row (game start-position row-num col-counter)
    (cond ((= col-counter -1) nil)
          (t (let*  (
                      (new-state-generated (make-a-move game (append start-position (list row-num col-counter))))
                      (end-position (list row-num col-counter))
                    )
                    (cond ((null new-state-generated) (--get-posible-states-from-pos-Row game start-position row-num (- col-counter 1)))
                            (t (append 
                                    (--get-posible-states-from-pos-Row game start-position row-num (- col-counter 1))
                                    (---create-state-move-struct new-state-generated start-position end-position)
                            ))
                    )  
              ))
    )
)

(defun ---create-state-move-struct (game start-position end-position)
    (list (append (list game) (list (append start-position end-position))))
)

; === Formiranje liste pozicija svih igracevih figura ===

(defun game-get-player-figures-positions (player table)
    (--get-player-fig-pos-Table player table 0)
)

(defun --get-player-fig-pos-Table (player table row-num)
    (cond ((null table) nil)
          (t (append 
                (--get-player-fig-pos-Row player (car table) row-num 0) 
                (--get-player-fig-pos-Table player (cdr table) (1+ row-num))
              ))
    )
)

(defun --get-player-fig-pos-Row (player row row-num col-counter)
    (cond ((null row) nil)
          ((equal (car row) player) 
                (cons   (list row-num col-counter) 
                        (--get-player-fig-pos-Row player (cdr row) row-num (1+ col-counter))
                ))
          (t (--get-player-fig-pos-Row player (cdr row) row-num (1+ col-counter)))
    )
)

; === Pomocne funkcije ===
;Kreira game strukturu
(defun create-game-state (table player-figure current-player)
	(append (list table) (list player-figure) (list current-player)))

;Odigrava potez na stanju. 
;Ako je potez validan vraca novo stanje, u suprotnom staro
(defun change-game-state (game move)
    (let (
            (new-state (make-a-move game move))
         )
         (cond 
            ((null new-state) (progn (display-invalid-move-msg) game))
            (t new-state))))

#| ======================== game-states.lisp :end ===========================|#
#| ==========================================================================|#
#| =========================================================================|#
#| ======================== game-table.lisp :begin =========================|#

(defun create-table (dimension)
	(cond 
		((<= dimension 6) nil)
		(t (append 
			(create-rows dimension 2 'X) 
			(create-rows dimension (- dimension 4) '-) 
			(create-rows dimension 2 'O)))))

(defun create-rows (dimension number player)
	(cond
		((zerop number) nil)
		(t (append (list (create-row dimension player)) (create-rows dimension (- number 1) player)))))

(defun create-row (dimension player)
	(make-list dimension :initial-element player))

(defun set-field (table row column value)
	(cond 
		((null table) nil)
		((zerop row) (append (list (replace-element value column (car table))) (cdr table)))
		(t (append (list (car table)) (set-field (cdr table) (- row 1) column value)))))

(defun get-field (table row column)
  (if (check-indexes row column (length table)) (nth column (nth row table))))

(defun clear-field (table row column)
	(set-field table row column '-))

(defun check-indexes (row column dimension)
  (and 
    (>= row 0) (>= column 0) 
    (< row dimension) (< column dimension)))

#| ======================== game-table.lisp :end ===========================|#
#| =========================================================================|#
#| =========================================================================|#
#| ======================== game.lisp :begin ===============================|#


(setq game-current-state nil)

;;; Inicijalizacija i pocetak partije
(defun start-game ()
    (progn
        (setq game-current-state (new-game))
        (game-loop)
    )
)

;;; Glavna petlja partije
(defun game-loop ()
    (loop 
        (progn
            (display-game game-current-state) 
            (let*   (
                        (player-input (get-player-move-input))
                        (game-new-state (change-game-state game-current-state (parse-move player-input)))
                        (is-gameover (gameover-check-if-won (get-table game-new-state) (get-current-player game-current-state)))
                    )
                    (cond   (is-gameover    (progn 
                                                (display-gameover (get-table game-new-state) (get-current-player game-current-state))
                                                (return)
                                            )
                            )
                            (t (setq game-current-state game-new-state))
                    )
            )
        )
    )        
)

;;; Inicijalizacija nove table i odabir figure
(defun new-game ()
	(create-game (player-input "Dimenzija table (min 7): ") (player-input "Vasa figura (X | O): ") 'X))

;#| Igra je stanje koje sadrzi tablu, figuru igraca i informaciju o tome ko je na potezu. 
;   Programski gledano igra je lista koja ima tri elementa: tablu, figuru igraca i igraca ko je na potezu.|#
(defun create-game (dimension player on-the-move)
	(cond
		((not (or (eq player 'X) (eq player 'O))) nil)
        (t (create-game-state (create-table dimension) player on-the-move))
    )
)

(defun next-player (game)
	(cond
		((eq (get-current-player game) 'X) 'O)
		((eq (get-current-player game) 'O) 'X)))

#| ======================== game.lisp :end =================================|#
#| =========================================================================|#
#| =========================================================================|#
#| ======================== utilities.lisp :begin ==========================|#



(defun player-input (message)
	(format t "~%~a" message)
	(read))

#| ======================== utilities.lisp :end ============================|#
#| =========================================================================|#
#| =========================================================================|#
#| ======================== utils-getters.lisp :begin ======================|#

; ### Getter funkcije ###

(defun get-table (game)
  (car game))

(defun get-player (game)
  (cadr game))

(defun get-current-player (game)
  (caddr game))

(defun get-table-row (table index)
	(nth index table))

(defun get-table-dimension (game)
  (length (get-table game))
)

(defun get-table-column (table index)
	(nth index (matrix-transpose table)))

(defun get-oponent-figure (figure)
    (if (equal figure 'X) 'O 'X))

(defun get-player-move-input ()
  (list (read-char) (read) (read-char) (read)))

; ### //Getter funkcije 

#| ======================== utils-getters.lisp :end ========================|#
#| =========================================================================|#
#| =========================================================================|#
#| ======================== utils-list.lisp :begin =========================|#

(defun replace-element (element index target-list)
  (cond
    ((null element) nil)
    ((null target-list) nil)
    ((zerop index) (append (list element) (cdr target-list)))
    (t (append (list (car target-list)) (replace-element element (- index 1) (cdr target-list))))))

;;; Broji koliko uzastopnih elemenata el ima od pocetka liste
(defun count-successive (list element)
    (cond ((null list) 0)
          ((not (equal (car list) element)) 0)
          (t (+ 1 (count-successive (cdr list) element)))
    )
)

;;; Provera da li lista sadrzi podlistu od n uzastopnih elemenata el
(defun check-list-has-n-successive (list element n)
    (cond ((null list) nil)
          (t (let ((counter (count-successive list element)))
                    (cond ((>= counter n) t)
                          (t (check-list-has-n-successive (member element (nthcdr counter list)) element n))
                    )
             )
          )
    )
)

;; Proverava da li su svi elementi u listi jednaki nekom elementu
(defun check-same-elements-in-list (target-list element)
	(cond 
		((null (cadr target-list)) (eq (car target-list) element))
		(t (and (eq element (car target-list)) (check-same-elements-in-list (cdr target-list) element)))))

#| ======================== utils-list.lisp :end ===========================|#
#| =========================================================================|#
#| =========================================================================|#
#| ======================== utils-matrix.lisp :begin =======================|#

;;; Vrati element matrice na poziciji i,j
(defun matrix-get-element (matrix i j)
    (nth j (nth i matrix))
)

;;; Proverava da li matrica/tabla sadrzi podlistu od n uzastopnih elemenata el
(defun matrix-check-has-n-successive (matrix element n)
    (cond ((null matrix) nil)
          ((check-list-has-n-successive (car matrix) element n) t)
          (t (matrix-check-has-n-successive (cdr matrix) element n))    
    )
)

; ### Transpozicija matrice ###

;;; [pomocna] Vraca listu prvih elemenata svih podlisti
(defun --matrix-transpose-cars (matrix)
  (cond ((null matrix) nil)
        (t (cons (car (car matrix)) (--matrix-transpose-cars (cdr matrix))))
  )
)

;;; [pomocna] Vraca listu cdr-ova svih podlisti 
(defun --matrix-transpose-cdrs (matrix)
  (cond ((null matrix) nil)
        (t (cons (cdr (car matrix)) (--matrix-transpose-cdrs (cdr matrix))))
  )
)

;;; Vrsi transpoziciju matrice
(defun matrix-transpose (matrix)
  (cond ((null matrix) nil)
        ((null (car matrix)) nil)
        (t (cons (--matrix-transpose-cars matrix) (matrix-transpose (--matrix-transpose-cdrs matrix))))
  )
)
; ### //Transpozicija matrice ###


; ### Generisanje diagonalnog pogleda matrice ###

;;; [pomocna] Vrati forward-diagonalu pocevsi od elementa matrix[row-index, column-index]
(defun --matrix-get-fdiag (matrix row-index column-index matrix-length)
    (cond ;;proveri validnost indeksa
          ((or (= row-index matrix-length) (= column-index -1)) nil)
          (t (cons (matrix-get-element matrix row-index column-index) 
                   (--matrix-get-fdiag matrix (1+ row-index) (1- column-index) matrix-length)))
    )
)

; [pomocna] Vrati backward-diagonalu pocevsi od elementa matrix[row-index, column-index]
(defun --matrix-get-bdiag (matrix row-index column-index matrix-length)
    (cond ((or (= row-index matrix-length) (= column-index matrix-length)) nil)
          (t (cons (matrix-get-element matrix row-index column-index) 
                   (--matrix-get-bdiag matrix (1+ row-index) (1+ column-index) matrix-length)))
    )
)

; [pomocna] Vrati sve forward-diagonale pocevsi od elementa matrix[row, column]
(defun --matrix-get-all-fdiag (matrix row-index col-index matrix-length)
    (cond ((< col-index (1- matrix-length)) (cons (--matrix-get-fdiag matrix 0 col-index matrix-length)
                                                  (--matrix-get-all-fdiag matrix 0 (1+ col-index) matrix-length)))
          
          ((< row-index matrix-length) (cons (--matrix-get-fdiag matrix row-index col-index matrix-length) 
                                              (--matrix-get-all-fdiag matrix (1+ row-index) col-index matrix-length)))                                
    )
)

; [pomocna] Vrati sve backward-diagonale pocevsi od elementa matrix[row, column]
(defun --matrix-get-all-bdiag (matrix row-index col-index matrix-length)
    (cond ((> col-index 0) (cons (--matrix-get-bdiag matrix 0 col-index matrix-length)
                                  (--matrix-get-all-bdiag matrix 0 (1- col-index) matrix-length)))
           ((< row-index matrix-length) (cons (--matrix-get-bdiag matrix row-index col-index matrix-length) 
                                              (--matrix-get-all-bdiag matrix (1+ row-index) col-index matrix-length)))                                
    )
)

; [glavna] Vrati forward diagonalni pogled matrice
(defun matrix-get-fdiag-view (matrix)
    (--matrix-get-all-fdiag matrix 0 0 (length (car matrix)))
)

; [glavna] Vrati backward diagonalni pogled matrice
(defun matrix-get-bdiag-view (matrix)
    (let ((matrix-length (length (car matrix))))
        (--matrix-get-all-bdiag matrix 0 (1- matrix-length) matrix-length)        
    )
)

; ### //Generisanje diagonalnog pogleda matrice ### ###

#| ======================== utils-matrix.lisp :end =========================|#
#| =========================================================================|#
#| =========================================================================|#
#| ======================== main.lisp :begin ===============================|#



(start-game)

#| ======================== main.lisp :end =================================|#
#| =========================================================================|#