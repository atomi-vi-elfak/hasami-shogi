#| =========================================================================|#
#| ======================== utilities.lisp :begin ==========================|#

(require "./utils-list.lisp")
(require "./utils-matrix.lisp")
(require "./utils-getters.lisp")


(defun player-input (message)
	(format t "~%~a" message)
	(read))

#| ======================== utilities.lisp :end ============================|#
#| =========================================================================|#
