#| =========================================================================|#
#| ======================== utils-matrix.lisp :begin =======================|#

;;; Vrati element matrice na poziciji i,j
(defun matrix-get-element (matrix i j)
    (nth j (nth i matrix))
)

;;; Proverava da li matrica/tabla sadrzi podlistu od n uzastopnih elemenata el
(defun matrix-check-has-n-successive (matrix element n)
    (cond ((null matrix) nil)
          ((check-list-has-n-successive (car matrix) element n) t)
          (t (matrix-check-has-n-successive (cdr matrix) element n))    
    )
)

; ### Transpozicija matrice ###
;; Zbog optimizacije promenjena implementacija
;; Preuzeta sa: 
;; https://forums.autodesk.com/t5/visual-lisp-autolisp-and-general/matrix-transposition/m-p/793695/highlight/true#M19353

(defun matrix-transpose (matrix)
    (apply 'mapcar (cons 'list matrix))
)
; ### //Transpozicija matrice ###


; ### Generisanje diagonalnog pogleda matrice ###

;;; [pomocna] Vrati forward-diagonalu pocevsi od elementa matrix[row-index, column-index]
(defun --matrix-get-fdiag (matrix row-index column-index matrix-length)
    (cond ;;proveri validnost indeksa
          ((or (= row-index matrix-length) (= column-index -1)) nil)
          (t (cons (matrix-get-element matrix row-index column-index) 
                   (--matrix-get-fdiag matrix (1+ row-index) (1- column-index) matrix-length)))
    )
)

; [pomocna] Vrati backward-diagonalu pocevsi od elementa matrix[row-index, column-index]
(defun --matrix-get-bdiag (matrix row-index column-index matrix-length)
    (cond ((or (= row-index matrix-length) (= column-index matrix-length)) nil)
          (t (cons (matrix-get-element matrix row-index column-index) 
                   (--matrix-get-bdiag matrix (1+ row-index) (1+ column-index) matrix-length)))
    )
)

; [pomocna] Vrati sve forward-diagonale pocevsi od elementa matrix[row, column]
(defun --matrix-get-all-fdiag (matrix row-index col-index matrix-length)
    (cond ((< col-index (1- matrix-length)) (cons (--matrix-get-fdiag matrix 0 col-index matrix-length)
                                                  (--matrix-get-all-fdiag matrix 0 (1+ col-index) matrix-length)))
          
          ((< row-index matrix-length) (cons (--matrix-get-fdiag matrix row-index col-index matrix-length) 
                                              (--matrix-get-all-fdiag matrix (1+ row-index) col-index matrix-length)))                                
    )
)

; [pomocna] Vrati sve backward-diagonale pocevsi od elementa matrix[row, column]
(defun --matrix-get-all-bdiag (matrix row-index col-index matrix-length)
    (cond ((> col-index 0) (cons (--matrix-get-bdiag matrix 0 col-index matrix-length)
                                  (--matrix-get-all-bdiag matrix 0 (1- col-index) matrix-length)))
           ((< row-index matrix-length) (cons (--matrix-get-bdiag matrix row-index col-index matrix-length) 
                                              (--matrix-get-all-bdiag matrix (1+ row-index) col-index matrix-length)))                                
    )
)

; [glavna] Vrati forward diagonalni pogled matrice
(defun matrix-get-fdiag-view (matrix)
    (--matrix-get-all-fdiag matrix 0 0 (length (car matrix)))
)

; [glavna] Vrati backward diagonalni pogled matrice
(defun matrix-get-bdiag-view (matrix)
    (let ((matrix-length (length (car matrix))))
        (--matrix-get-all-bdiag matrix 0 (1- matrix-length) matrix-length)        
    )
)

; ### //Generisanje diagonalnog pogleda matrice ### ###

#| ======================== utils-matrix.lisp :end =========================|#
#| =========================================================================|#
