#| =========================================================================|#
#| ======================== utils-getters.lisp :begin ======================|#

; ### Getter funkcije ###

(defun get-table (game)
  (car game))

(defun get-user-figure (game)
  (cadr game))

(defun get-current-player (game)
  (caddr game))

(defun get-table-row (table index)
	(nth index table))

(defun get-table-dimension (game)
  (length (get-table game))
)

(defun get-table-column (table index)
	(nth index (matrix-transpose table)))

(defun get-oponent-figure (figure)
    (if (equal figure 'X) 'O 'X))

(defun get-player-move-input ()
  (list (read-char) (read) (read-char) (read)))

; ### //Getter funkcije 

#| ======================== utils-getters.lisp :end ========================|#
#| =========================================================================|#
