#| =========================================================================|#
#| ======================== utils-list.lisp :begin =========================|#

(defun replace-element (element index target-list)
  (cond
    ((null element) nil)
    ((null target-list) nil)
    ((zerop index) (append (list element) (cdr target-list)))
    (t (append (list (car target-list)) (replace-element element (- index 1) (cdr target-list))))))

;;; Broji koliko uzastopnih elemenata el ima od pocetka liste
(defun count-successive (list element)
    (cond ((null list) 0)
          ((not (equal (car list) element)) 0)
          (t (+ 1 (count-successive (cdr list) element)))
    )
)

;;; Provera da li lista sadrzi podlistu od n uzastopnih elemenata el
(defun check-list-has-n-successive (list element n)
    (cond ((null list) nil)
          (t (let ((counter (count-successive list element)))
                    (cond ((>= counter n) t)
                          (t (check-list-has-n-successive (member element (nthcdr counter list)) element n))
                    )
             )
          )
    )
)

;; Proverava da li su svi elementi u listi jednaki nekom elementu
(defun check-same-elements-in-list (target-list element)
	(cond 
		((null (cadr target-list)) (eq (car target-list) element))
		(t (and (eq element (car target-list)) (check-same-elements-in-list (cdr target-list) element)))))

#| ======================== utils-list.lisp :end ===========================|#
#| =========================================================================|#
