#!/bin/bash

DEFUALT_OUTPUT_FILENAME=hasami-shogi
GAME_FILES=game/*
UTILITIES_FILES=utilities/*

GEN_DATE=$(date)
GIT_LAST_COMMIT=$(git rev-parse HEAD)
BIN_DIR=bin/
RUN_FILE=run.sh


if [ -z "$1" ]
  then
    OUT_FILENAME=$DEFUALT_OUTPUT_FILENAME
  else
    OUT_FILENAME=$1
fi

OUT_FILE=$BIN_DIR$OUT_FILENAME.lisp
OUT_COMPILE=$BIN_DIR$OUT_FILENAME.fas

echo "==================================="
echo "       Hasami Shogi Builder        "
echo "==================================="
echo "OUT_FILE = $OUT_FILE"
echo "GAME_DIR = $GAME_FILES"
echo "UTILITIES_DIR = $UTILITIES_FILES"
echo "DATE = $GEN_DATE"
echo "LAST_COMMIT = $GIT_LAST_COMMIT"
echo "==================================="

echo "Cleaning $BIN_DIR ..."
if [ ! -d $BIN_DIR ]; then
    echo "ERROR: BIN DIR DOES NOT EXISTS"
    exit$OUT_FILE$OUT_FILE
fi
cd $BIN_DIR
find . ! -name '.gitkeep' -type f -exec rm -f {} +
cd ..

echo "Generating combined file: $OUT_FILE"
echo > $OUT_FILE

echo "#|" >> $OUT_FILE

echo "Generated on: $GEN_DATE" >>  $OUT_FILE
echo "Last commit: $GIT_LAST_COMMIT" >> $OUT_FILE

echo "|#" >> $OUT_FILE
echo "" >> $OUT_FILE 

for f in $GAME_FILES
do
  echo "Processing: $f..."
  grep -vwE "(require)" $f >> $OUT_FILE
done

for f in $UTILITIES_FILES
do
  echo "Processing: $f..."
  grep -vwE "(require)" $f >> $OUT_FILE
done

echo "Processing: main.lisp..."
grep -vwE "(require)" main.lisp >> $OUT_FILE

clisp -q -c $OUT_FILE

echo "Generating run file..."

printf "#!/bin/bash\n\n" > $RUN_FILE
printf "FILE=$OUT_COMPILE\n\n" >> $RUN_FILE
printf "if [ ! -f \$FILE ]; then\n" >> $RUN_FILE
printf "\techo \"File \$FILE not found. Forgot to built?\"\n" >> $RUN_FILE
printf "\texit\n" >> $RUN_FILE
printf "fi\n\n" >> $RUN_FILE
printf "clisp -q \$FILE\n" >> $RUN_FILE

echo "==================================="
echo "             Finished              "
echo "==================================="