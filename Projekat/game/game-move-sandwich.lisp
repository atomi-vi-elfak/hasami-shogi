#| =========================================================================|#
#| ======================== game-move-sandwich.lisp :begin =================|#

(defun make-sandwich (table row-end column-end figure)
	(let* (
		(cleared-right (make-sandwich-right table row-end column-end figure)) 
		(cleared-left (make-sandwich-left cleared-right row-end column-end figure)) 
		(cleared-top (make-sandwich-top cleared-left row-end column-end figure))
		(cleared-bottom (make-sandwich-bottom cleared-top row-end column-end figure)))
		cleared-bottom))

;;; Primenjuje sendvic obradu na elementima udesno od odigrane figure
(defun make-sandwich-right (table row-end column-end figure)
	(let ((row (make-sandwich-in-list (get-table-row table row-end) column-end figure)))
		(replace-element row row-end table)))

;;; Primenjuje sendvic obradu nad elementima ulevo od odigrane figure
(defun make-sandwich-left (table row-end column-end figure)
	(let ((row (reverse (make-sandwich-in-list (reverse (get-table-row table row-end)) (- (length table) column-end 1) figure))))
		(replace-element row row-end table)))

;;; Primenjuje sendvic obradu nad elementima iznad od odigrane figure
(defun make-sandwich-top (table row-end column-end figure)
	(let ((column (reverse (make-sandwich-in-list (reverse (get-table-column table column-end)) (- (length table) row-end 1) figure))))
		(matrix-transpose (replace-element column column-end (matrix-transpose table)))))

;;; Primenjuje sendvic obradu nad elementima ispod od odigrane figure
(defun make-sandwich-bottom (table row-end column-end figure)
	(let ((column (make-sandwich-in-list (get-table-column table column-end) row-end figure)))
		(matrix-transpose (replace-element column column-end (matrix-transpose table)))))

;;; Sklanja protivnicke figure u sendvicu ako je sendvic ostvaren. Uopsteni algoritam, radi nad listom.
(defun make-sandwich-in-list (row column-index figure)
	(cond
		((not (equalp (nth column-index row) figure)) row)
		(t (let 
			((sandwich-count (check-sandwich (subseq row column-index (length row)) figure)))
			(if (> sandwich-count 0) (clear-fields row (1+ column-index) (+ 1 column-index sandwich-count)) row)))))

;;; Prebrojava koliko protivnickih figura ima u potencialnom sendvicu
;	Ako je broj negativan => sendvic nije formiran.
(defun check-sandwich (row figure)
  (cond
    ((null (cadr row)) -10000)
    ((equalp (cadr row) figure) 0)
    ((equalp (cadr row) '-) -10000)
    ((not (equalp (cadr row) figure)) (1+ (check-sandwich (cdr row) figure)))))

(defun clear-fields (row start end)
	(cond
		((null row) nil)
		((zerop end) row)
		((zerop start) (cons '- (clear-fields (cdr row) start (1- end))))
		(t (cons (car row) (clear-fields (cdr row) (1- start) (1- end))))))

#| ======================== game-move-sandwich.lisp :end ===================|#
#| =========================================================================|#
