#| =========================================================================|#
#| ======================== game-display.lisp :begin =======================|#

(defun display-game (game)
	(progn 
		(format t "~%")
		(display-table (car game))
		(display-player-status (caddr game))))

(defun display-gameover (table winner)
	(progn
		(display-table table)
		(format t "~%~%Kraj igre => Pobednik je: ~a!" winner)))

(defun display-table (table)
	(mapcar #'display-row (append 
		(list (create-columns-indicator-row (length table)))
		(insert-rows-indicator table (length table)))))

(defun display-player-status (player)
	(format t "~%~%Igrac ~a je na potezu: " player))

(defun display-invalid-move-msg ()
	(format t "~%~% ==== Los potez! ==== ~%~%")
)

(defun create-columns-indicator-row (dimension)
	(cond 
		((zerop dimension) (list #\#)) 
		(t (append (create-columns-indicator-row (- dimension 1)) (list dimension)))))

(defun insert-rows-indicator (table dimension)
	(cond
		((null table) nil)
		(t (append (list (insert-row-indicator (car table) (- dimension (length table)))) (insert-rows-indicator (cdr table) dimension)))))

(defun insert-row-indicator (row offset)
	(cons (code-char (+ (char-code #\A) offset)) row))

(defun display-row (row)
	(progn 
		(mapcar (lambda (element) (format t "~a " element)) row))
	    (format t "~%"))

#| ======================== game-display.lisp :end =========================|#
#| =========================================================================|#
