#| =========================================================================|#
#| ======================== game-table.lisp :begin =========================|#

(defun create-table (dimension)
	(cond 
		((<= dimension 6) nil)
		(t (append 
			(create-rows dimension 2 'X) 
			(create-rows dimension (- dimension 4) '-) 
			(create-rows dimension 2 'O)))))

(defun create-rows (dimension number player)
	(cond
		((zerop number) nil)
		(t (append (list (create-row dimension player)) (create-rows dimension (- number 1) player)))))

(defun create-row (dimension player)
	(make-list dimension :initial-element player))

(defun set-field (table row column value)
	(cond 
		((null table) nil)
		((zerop row) (append (list (replace-element value column (car table))) (cdr table)))
		(t (append (list (car table)) (set-field (cdr table) (- row 1) column value)))))

(defun get-field (table row column)
  (if (check-indexes row column (length table)) (nth column (nth row table))))

(defun clear-field (table row column)
	(set-field table row column '-))

(defun check-indexes (row column dimension)
  (and 
    (>= row 0) (>= column 0) 
    (< row dimension) (< column dimension)))

#| ======================== game-table.lisp :end ===========================|#
#| =========================================================================|#
