#| =========================================================================|#
#| ======================== game-over.lisp :begin ==========================|#

;;; Testira da li je igrac pobedio
(defun gameover-check-if-won (table figure)
    (or
        (--gameover-check-nofigures table (get-oponent-figure figure))   
        (--gameover-check-win-vert table figure)
        (--gameover-check-win-diag table figure)
    )
)

;;; Testira da li je neko pobedio. Vraca pobednika ili nil
(defun gameover-check-if-any-won (table)
    (cond ((gameover-check-if-won table 'X) 'X)
          ((gameover-check-if-won table 'O) 'O)
    )
)

;;; Uslov: Spojio 5 figura u vertikali
(defun --gameover-check-win-vert (table figure)
    (cond ((equal figure 'X) (matrix-check-has-n-successive (matrix-transpose (cddr table)) figure 5))
          ((equal figure 'O) (matrix-check-has-n-successive (matrix-transpose (cddr (reverse table))) figure 5))
    )
)

; ;;; Uslov: Spojio 5 figura u horizontali
; (defun --gameover-check-win-horiz (table figure)
;     (cond ((equal figure 'X) (matrix-check-has-n-successive (cddr table) figure 5))
;           ((equal figure 'O) (matrix-check-has-n-successive (cddr (reverse table)) figure 5))
;     )
; )

;;; Uslov: Spojio 5 figura u nekoj dijagonali
(defun --gameover-check-win-diag (table figure)
    (or (--gameover-check-win-fdiag table figure) (--gameover-check-win-bdiag table figure))
)

;;; PodUslov: Spojio 5 figura u forward-diagonali /
; TODO: optimise
(defun --gameover-check-win-fdiag (table figure)
    (cond ((equal figure 'X) (matrix-check-has-n-successive (matrix-get-fdiag-view (cddr table)) figure 5))
          ((equal figure 'O) (matrix-check-has-n-successive (matrix-get-fdiag-view (reverse (cddr (reverse table)))) figure 5))
    )
)

;;; PodUslov: Spojio 5 figura u backward-dijagonali \
; TODO: optimise
(defun --gameover-check-win-bdiag (table figure)
    (cond ((equal figure 'X) (matrix-check-has-n-successive (matrix-get-bdiag-view (cddr table)) figure 5))
          ((equal figure 'O) (matrix-check-has-n-successive (matrix-get-bdiag-view (reverse (cddr (reverse table)))) figure 5))
    )
)

;;; Uslov: Ima manje od 5 figura
; TODO: optimise
(defun --gameover-check-nofigures (table figure)
    (> 5 (count-player-figures table figure))
)

(defun count-player-figures (table figure)
    (reduce #'+ (mapcar (lambda (table-row) (count figure table-row)) table)))

#| ======================== game-over.lisp :end ============================|#
#| =========================================================================|#
