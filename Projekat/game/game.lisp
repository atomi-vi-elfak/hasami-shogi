#| =========================================================================|#
#| ======================== game.lisp :begin ===============================|#

(require "./game-table.lisp")
(require "./game-over.lisp")
(require "./game-display.lisp")
(require "./game-move.lisp")
(require "./game-states.lisp")
(require "./game-ai.lisp")

(defvar *conf_depth*)
(defvar game-current-state)


(setq *conf_depth* 2)
(setq game-current-state nil)

;;; Inicijalizacija i pocetak partije
(defun start-game ()
    (progn
        (setq game-current-state (new-game))
        (game-loop)
    )
)

;;; Glavna petlja partije
(defun game-loop ()
    (loop 
        (progn
            (display-game game-current-state) 
            (let*   (
                        (player-input (get-move game-current-state))
                        (game-new-state (change-game-state game-current-state (parse-move player-input)))
                        (is-gameover (gameover-check-if-won (get-table game-new-state) (get-current-player game-current-state)))
                    )
                    (cond   (is-gameover    (progn 
                                                (display-gameover (get-table game-new-state) (get-current-player game-current-state))
                                                (return)
                                            )
                            )
                            (t (setq game-current-state game-new-state))
                    )
            )
        )
    )        
)

(defun get-move (game-current-state)
    (cond
        ((eq (get-current-player game-current-state) (get-user-figure game-current-state)) (get-player-move-input))
        (t (reverse-parse-move (cadar (alphabeta game-current-state *conf_depth*))))))

;;; Inicijalizacija nove table i odabir figure
(defun new-game ()
	(create-game (player-input "Dimenzija table (min 7): ") (player-input "Vasa figura (X | O): ") 'X))

;#| Igra je stanje koje sadrzi tablu, figuru igraca i informaciju o tome ko je na potezu. 
;   Programski gledano igra je lista koja ima tri elementa: tablu, figuru igraca i igraca ko je na potezu.|#
(defun create-game (dimension player on-the-move)
	(cond
		((not (or (eq player 'X) (eq player 'O))) nil)
        (t (create-game-state (create-table dimension) player on-the-move))
    )
)

(defun next-player (game)
	(cond
		((eq (get-current-player game) 'X) 'O)
		((eq (get-current-player game) 'O) 'X)))

#| ======================== game.lisp :end =================================|#
#| =========================================================================|#
