#| =========================================================================|#
#| ======================== game-move.lisp :begin ==========================|#

(require "./game-move-sandwich.lisp")

;#| Validacije #|row-start column-start row-end column-end|#|#
(defun check-valid-move (game move)
	(and 
		(check-start-position game (nth 0 move) (nth 1 move)) 
		(or (check-move-position game (nth 0 move) (nth 1 move) (nth 2 move) (nth 3 move))
			(check-jump-move (get-table game) (nth 0 move) (nth 1 move) (nth 2 move) (nth 3 move)))))

(defun check-start-position (game row column)
	(eq (get-current-player game) (get-field (get-table game) row column)))

(defun check-move-position (game row-start column-start row-end column-end)
	(and 
		(check-empty-position (get-table game) row-end column-end)
		(check-no-figure-between (get-table game) row-start column-start row-end column-end)))

(defun check-empty-position (table row column)
	(eq (get-field table row column) '-))

;#| Needs refactoring |#
(defun check-no-figure-between (table row-start column-start row-end column-end)
	(cond
		((eq row-start row-end)
			(cond ((eq (abs (- column-end column-start)) 1) t)
				(t (check-same-elements-in-list (subseq (get-table-row table row-start) (1+ (min column-start column-end)) (max column-end column-start)) '-))))
		((eq column-start column-end)
			(cond ((eq (abs (- row-start row-end)) 1) t)
				(t (check-same-elements-in-list (subseq (get-table-column table column-start) (1+ (min row-start row-end)) (max row-start row-end)) '-))))))
;#| Validacije kraj |#

;#| Move: row-start column-start row-end column-end |#
(defun make-a-move (game move)
	(if (check-valid-move game move)
		(let* (
				(move-played (set-field (clear-field (get-table game) (nth 0 move) (nth 1 move)) (nth 2 move) (nth 3 move) (get-current-player game)))
				(sandwich-move-played (make-sandwich move-played (nth 2 move) (nth 3 move) (get-current-player game)))
			  )			
				(create-game-state sandwich-move-played (get-user-figure game) (next-player game)))))

;#| Input je oblika: ((row-start column-start) (row-end column-end)) 
;   Dodatna validacija|#
(defun check-input (input)
	(and 
		(listp input)
		(eq (length input) 2)
		(listp (car input))
		(listp (cadr input))
		(eq (length (car input)) 2)
		(eq (length (cadr input)) 2)))

;#| Konvetuje input oblika A 1 C 1 => 0 0 2 0|#
(defun parse-move (move)
	(cond
		((null move) nil)
		(t (list (index-of-char (nth 0 move)) (1- (nth 1 move)) (index-of-char (nth 2 move)) (1- (nth 3 move))))))

(defun reverse-parse-move (move)
	(cond
		((null move) nil)
		(t (list (char-of-index (nth 0 move)) (1+ (nth 1 move)) (char-of-index (nth 2 move)) (1+ (nth 3 move))))))

(defun index-of-char (char-input)
	(- (char-code char-input) (char-code #\A)))

(defun char-of-index (index-input)
	(code-char (+ (char-code #\A) index-input)))

(defun check-jump-move (table row-start column-start row-end column-end) 
  (cond
  	((null table) nil)
    (t (or 
        (check-jump-move-vertical table row-start column-start row-end column-end)
        (check-jump-move-horizontal table row-start column-start row-end column-end)))))

(defun check-jump-move-vertical (table row-start column-start row-end column-end)
  (and 
    (eq column-start column-end)
    (eq (abs (- row-start row-end)) 2) 
    (eq (get-field table row-end column-start) '-)))

(defun check-jump-move-horizontal (table row-start column-start row-end column-end)
    (and 
    (eq row-start row-end)
    (eq (abs (- column-start column-end)) 2)
    (eq (get-field table row-start column-end) '-)))

#| ======================== game-move.lisp :end ============================|#
#| =========================================================================|#
