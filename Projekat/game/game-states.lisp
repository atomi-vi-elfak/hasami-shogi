#| =========================================================================|#
#| ======================== game-states.lisp :begin ========================|#

;;Vraca: ( (game move) (game move) )
(defun generate-next-possible-states (game)
    (reduce #'append 
        (mapcar (lambda (current-position) (get-posible-states-from-position game current-position)) 
                        (game-get-player-figures-positions (get-current-player game) (get-table game))
        )
    )
)

;; === Formiranje liste svih mogucih poteza za jednu figuru ===
(defun get-posible-states-from-position (game start-position)
    (--get-posible-states-from-pos-Table game start-position (get-table-dimension game) (get-table-dimension game)) ;;2lazy4let
)

(defun --get-posible-states-from-pos-Table (game start-position row-counter table-size)
    (cond ((= row-counter -1) nil)
          (t (append    (--get-posible-states-from-pos-Table game start-position (1- row-counter) table-size)
                        (--get-posible-states-from-pos-Row game start-position row-counter table-size)
          ))
    )
)

(defun --get-posible-states-from-pos-Row (game start-position row-num col-counter)
    (cond ((= col-counter -1) nil)
          (t (let*  (
                      (new-state-generated (make-a-move game (append start-position (list row-num col-counter))))
                      (end-position (list row-num col-counter))
                    )
                    (cond ((null new-state-generated) (--get-posible-states-from-pos-Row game start-position row-num (- col-counter 1)))
                            (t (append 
                                    (--get-posible-states-from-pos-Row game start-position row-num (- col-counter 1))
                                    (---create-state-move-struct new-state-generated start-position end-position)
                            ))
                    )  
              ))
    )
)

(defun ---create-state-move-struct (game start-position end-position)
    (list (append (list game) (list (append start-position end-position))))
)

; === Formiranje liste pozicija svih igracevih figura ===

(defun game-get-player-figures-positions (player table)
    (--get-player-fig-pos-Table player table 0)
)

(defun --get-player-fig-pos-Table (player table row-num)
    (cond ((null table) nil)
          (t (append 
                (--get-player-fig-pos-Row player (car table) row-num 0) 
                (--get-player-fig-pos-Table player (cdr table) (1+ row-num))
              ))
    )
)

(defun --get-player-fig-pos-Row (player row row-num col-counter)
    (cond ((null row) nil)
          ((equal (car row) player) 
                (cons   (list row-num col-counter) 
                        (--get-player-fig-pos-Row player (cdr row) row-num (1+ col-counter))
                ))
          (t (--get-player-fig-pos-Row player (cdr row) row-num (1+ col-counter)))
    )
)

; === Pomocne funkcije ===
;Kreira game strukturu
(defun create-game-state (table player-figure current-player)
	(append (list table) (list player-figure) (list current-player)))

;Odigrava potez na stanju. 
;Ako je potez validan vraca novo stanje, u suprotnom staro
(defun change-game-state (game move)
    (let (
            (new-state (make-a-move game move))
         )
         (cond 
            ((null new-state) (progn (display-invalid-move-msg) game))
            (t new-state))))

#| ======================== game-states.lisp :end ===========================|#
#| ==========================================================================|#
