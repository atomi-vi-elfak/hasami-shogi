#| =========================================================================|#
#| ======================== game-ai.lisp :begin ============================|#
(require "Inference_engine.lisp")

;; ====================== MINMAX sa ALPHABETA odsecanje ====================

(defun alphabeta (game depth)
    (time (--alphabeta (car (---create-state-move-struct game nil nil)) depth -10000 +10000 (equal (get-user-figure game) 'O))))


;; Ulazna struktura ((game) (move))
;; Inicijalno (move) je prazna lista - za pocetno stanje
(defun --alphabeta (game-move-state depth alpha beta my-move)
    (cond 
        ((zerop depth) (list game-move-state (evaluate (caar game-move-state) (get-user-figure (car game-move-state)))))
        (t     
            (let 
                (
                    (next-states (generate-next-possible-states (car game-move-state)))
                    (best-move nil)
                    (best-value nil)
                    (tmp nil)
                )
                (cond
                    ((null next-states) (list game-move-state (evaluate (caar game-move-state) (get-user-figure (car game-move-state)))))
                    (t (if my-move 
                            (progn 
                                (setf best-value -10000)
                                (mapcar (lambda (x) 
                                    (progn
                                        (setf tmp (--alphabeta x (1- depth) alpha beta nil)) 
                                        (if (> (cadr tmp) best-value) (progn (setf best-value (cadr tmp)) (setf best-move x)))
                                        (setf alpha (max alpha best-value))
                                        (if (>= alpha beta) (return-from --alphabeta (list best-move best-value)))
                                    )) next-states)
                                (list best-move best-value))
                            (progn 
                                (setf best-value 10000)
                                (mapcar (lambda (x) 
                                    (progn
                                        (setf tmp (--alphabeta x (1- depth) alpha beta t)) 
                                        (if (< (cadr tmp) best-value) (progn (setf best-value (cadr tmp)) (setf best-move x)))
                                        (setf beta (min beta best-value))
                                        (if (>= alpha beta) (return-from --alphabeta (list best-move best-value)))
                                    )) next-states)
                                (list best-move best-value)))))))))


;; ================================ MINMAX =================================

(defun minmax (game depth)
    (--minmax (car (---create-state-move-struct game nil nil)) depth (equal (get-user-figure game) 'O)))

(defun --minmax (game-move-state depth my-move)
    (let ((next-states (generate-next-possible-states (car game-move-state)))
            (function (if my-move 'max-state 'min-state)))
        (cond ((or (zerop depth) (null next-states))
                (list game-move-state (evaluation (caar game-move-state))))
              (t (apply function (list (mapcar (lambda (x)
                (--minmax x (1- depth)
                        (not my-move))) next-states)))))))

(defun max-state (states-with-values)
    (max-state-i (cdr states-with-values) (car states-with-values)))

(defun max-state-i (states-with-values state-with-value)
    (cond 
        ((null states-with-values) state-with-value)
        ((> (cadar states-with-values) (cadr state-with-value))
            (max-state-i (cdr states-with-values) (car states-with-values)))
        (t (max-state-i (cdr states-with-values) state-with-value))))

(defun min-state (states-with-values)
    (min-state-i (cdr states-with-values) (car states-with-values)))

(defun min-state-i (states-with-values state-with-value)
    (cond 
        ((null states-with-values) state-with-value)
        ((< (cadar states-with-values) (cadr state-with-value))
            (min-state-i (cdr states-with-values) (car states-with-values)))
        (t (min-state-i (cdr states-with-values) state-with-value))))


;; ============================== EVALUACIJA ===============================

(defun !eq (x y)
    (eq x y))

(defun !ne (x y)
    (not (eq x y)))

(defun evaluate (table player)
    (prepare-knowledge (set-rules) (set-facts table player) 10) 
    (+  
        (cadaar (infer '(Evaluate ?value 'sandwich-move 'X)))
        (if (equal player 'X)
            (cadaar (infer '(Evaluate ?value 'victory 'X)))
            (cadaar (infer '(Evaluate ?value 'victory 'O))))
        ))

(defun set-rules ()
    '(
            (if (Ready-for-evaluation ?value ?param ?player)
                then (Evaluate ?value ?param ?player))
            
            (if (and (Figure-difference ?value) (!eq ?param 'sandwich-move) (!eq ?player 'X))
                then (Ready-for-evaluation ?value ?param ?player))

            (if (and (Won-X ?value) (!eq ?param 'victory) (!eq ?player 'X) (!ne ?value nil))
                then (Ready-for-evaluation +32000 ?param ?player))

            (if (and (Won-X ?value) (!eq ?param 'victory) (!eq ?player 'X) (!eq ?value nil))
                then (Ready-for-evaluation 0 ?param ?player))

            (if (and (Won-O ?value) (!eq ?param 'victory) (!eq ?player 'O) (!ne ?value nil))
                then (Ready-for-evaluation -32000 ?param ?player))

            (if (and (Won-O ?value) (!eq ?param 'victory) (!eq ?player 'O) (!eq ?value nil))
                then (Ready-for-evaluation 0 ?param ?player))
           ))

(defun set-facts (table player)
  (list
        (list 'Figure-difference (figure-number-difference table 10))
        (list 'Won-X (gameover-check-if-won table 'X))
        (list 'Won-O (gameover-check-if-won table 'O))
    ))

(defun figure-number-difference (table factor)
    (* (- (count-player-figures table 'X) (count-player-figures table 'O)) factor))

#| ======================== game-ai.lisp :end ==============================|#
#| =========================================================================|#
